'''
Unit tests for basic module functionality
'''

import pytest

from simple_scrapper.wsgi import wsgi_app
from simple_scrapper.scrapper import Scrapper


@pytest.fixture
def client():
    return wsgi_app.test_client()


def test_ordinary_get(client):
    ''' Try to get unposted task '''
    get_response = client.get('/tag_counter/nonexistent_id')
    assert get_response.status_code == 203
    assert get_response.json['state'] == 'PENDING'


def test_good_post(client):
    ''' Try to make a valid POST request '''
    post_response = client.post('/tag_counter', data={'url': 'http://ya.ru'})
    assert post_response.status_code == 201


def test_bad_schema_post(client):
    ''' POST with unsupported URI schema '''
    post_response = client.post('/tag_counter', data={'url': 'bad://example.com'})
    assert post_response.status_code == 406
    assert 'error' in post_response.json
    assert 'schema is unsupported' in post_response.json['error']


def test_bad_url_post(client):
    ''' POST with invalid URI '''
    post_response = client.post('/tag_counter', data={'url': 'http://ex ample.com'})
    assert post_response.status_code == 406
    assert 'error' in post_response.json
    assert 'Malformed locator' in post_response.json['error']


# TODO Test with API using MOC
def test_scrapper():
    '''Run scrapper on simple HTML document'''
    scrapper = Scrapper()
    scrapper.init_from_string(
        '''
        <html>
          <head>
            <meta/>
          </head>
          <body>
            <div>
              <a/>
            </div>
            <div>
              <div>
                <a/>
                <a/>
                <a/>
                <a/>
              </div>
            </div>
          </body>
        </html>
        ''')

    expected = {
        'html': {'count': 1, 'nested': 11},
        'head': {'count': 1, 'nested': 1},
        'meta': {'count': 1, 'nested': 0},
        'body': {'count': 1, 'nested': 8},
        'div': {'count': 3, 'nested': 10},
        'a': {'count': 5, 'nested': 0}
    }

    assert scrapper.process() == expected
