FROM python:3.9-alpine
ARG additional_pkgs=gunicorn
LABEL description="Pre-employment test for SERVICEHUB"
COPY setup.py /srv/app/
COPY simple_scrapper/ /srv/app/simple_scrapper/
WORKDIR /srv/app/
ENTRYPOINT ["python"]
CMD ["-m", "gunicorn", "simple_scrapper.wsgi:wsgi_app", "--bind", "0.0.0.0"]
RUN pip install --no-cache-dir -e . $additional_pkgs
