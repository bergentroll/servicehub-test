﻿#!/usr/bin/env python3

from setuptools import find_packages
from setuptools import setup

setup(
    name='simple-scrapper',
    python_requires='>=3.9',
    packages=find_packages(),
    license='CC0',
    description='Test task for SERVICEHUB',
    install_requires=[
        'Flask>=2<3',
        'beautifulsoup4>=4.9',
        'celery>=5.1<6',
        'flask-swagger>=0.2<1.0',
        'requests>=2.26<3',
        'rfc3987>=1.3.8<2',
    ],
)
