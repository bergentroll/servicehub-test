'''
REST API for Scrapper
'''

import rfc3987

from flask import Flask, jsonify, request, send_from_directory
from flask_swagger import swagger
from typing import Dict, Tuple
from urllib.parse import urlparse

from simple_scrapper.tasks import process_page

wsgi_app = Flask('simple_scrapper')


@wsgi_app.route("/schema")
def swagger_endpoint():
    swagger_instance = swagger(wsgi_app)
    swagger_instance['info']['title'] = "Simple scrapper API"
    swagger_instance['info']['version'] = None
    return jsonify(swagger_instance)


@wsgi_app.route('/')
def hello():
    '''
    Get simple forms to make requests
    '''
    return send_from_directory('static', 'index.html')


@wsgi_app.route('/tag_counter/<string:task_id>', methods=['GET'])
def get_result(task_id: str) -> Tuple[Dict, int]:
    '''
    Getting scrapping task result
    Method return state of task found by task_id and result if appliable
    Please note than "PENDING" state may mean "No such task"
    ---
    parameters:
      - name: task_id
        in: body
        requred: true
        schema:
          type: string
    responses:
      200:
        description: Result is available
        content:
          application/json:
            schema:
             type: object
             properties:
                state:
                  type: string
                  description: State of requested task
                  enum:
                    - PENDING
                    - STARTED
                    - SUCCESS
                    - REVOKED
                    - FAILURE
                result:
                  type: object
                  description: Result of task execution
      203:
        description: Task is in queu or does not exist
        content:
          $ref: '#/responses/200/content'
      500:
        description: Task had been failed
        content:
          $ref: '#/responses/200/content'
    '''
    status_code = 200
    task = process_page.AsyncResult(task_id)

    if task.status == 'PENDING':
        status_code = 203

    result = task.result

    if issubclass(type(result), Exception):
        result = {'error': str(result)}
        status_code = 500
    response = {
        'state': task.state,
        'result': result,
    }

    return response, status_code


@wsgi_app.route('/tag_counter', methods=['POST'])
def create_task() -> Tuple[Dict, int]:
    '''
    Pending scrapping task
    Method creates an async task for scrapping page by given URL
    ---
    parameters:
      - name: url
        in: path
        requred: true
        schema:
          type: string
          format: url
    responses:
      201:
        description: Task has been created
        content:
          application/json:
            schema:
             type: object
             properties:
                task_id:
                  type: string
                  description: ID of created task
      406:
        description: Parameters has been decided invalid
        content:
          application/json:
            schema:
              type: object
              properties:
                error:
                  type: string
                  description: Error message
    '''
    url = request.form['url']

    if not rfc3987.match(url, 'absolute_IRI'):
        return {'error': 'Malformed locator'}, 406

    if urlparse(url).scheme not in ['http', 'https']:
        return (
            {'error': "Locator schema is unsupported, only {'http', 'https'} are valid"},
            406)

    task = process_page.delay(url)
    return {'task_id': task.id}, 201
