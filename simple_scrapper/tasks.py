'''
Declaration of Celery jobs
'''

import logging

from simple_scrapper.celery import celery_app
from simple_scrapper.scrapper import Scrapper


@celery_app.task
def process_page(url: str) -> dict:
    logger = logging.getLogger(__name__)
    logger.info(f'Run scrapping task for url {url}')

    scrapper = Scrapper()

    scrapper.init_from_remote(url)
    return scrapper.process()
