'''
Celery application for asynchronous tasks
'''

import os

from celery import Celery

celery_app = Celery('simple_scrapper', task_track_started=True)

celery_app.autodiscover_tasks(['simple_scrapper'])
celery_app.config_from_object('simple_scrapper.celeryconfig')

celery_broker_override = os.environ.get('CELERY_BROKER_URL')
if celery_broker_override:
    celery_app.conf.broker = celery_broker_override

if __name__ == '__main__':
    celery_app.start()
