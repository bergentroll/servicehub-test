'''
Standard Celery config file
'''

broker_url = 'amqp://localhost:5672'
result_backend = 'rpc://'
broker_connection_timeout = 5
