﻿'''
Provides class for processing remote web pages
'''

import bs4
import requests

from typing import Dict, Optional

ResultType = Dict[str, Dict[str, int]]


class Scrapper:
    '''
    Parse html page and count tags amount
    '''

    class ScrapperError(RuntimeError):
        ...

    def __init__(self):
        self._data: Optional[bs4.BeautifulSoup] = None

    def init_from_remote(self, url: str) -> None:
        '''
        Initialize an instance with page by url

        :param url: Link to desired html page
        :raises Scrapper.ScrapperError: On fail to get remote resource
        '''
        try:
            responce = requests.get(url)
        except requests.exceptions.RequestException as error:
            raise Scrapper.ScrapperError(str(error)) from error
        if not responce:
            msg = f'Failed to get a document: {responce.status_code} {responce.reason}'
            raise Scrapper.ScrapperError(msg)
        self._data = bs4.BeautifulSoup(responce.text, features='html.parser')

    def init_from_string(self, page: str) -> None:
        '''
        Initialize an instance with string

        :param page: String with html text
        '''
        self._data = bs4.BeautifulSoup(page, features='html.parser')

    # TODO Make non-recursive
    @staticmethod
    def _traverse(node: bs4.element.Tag, result: dict) -> int:
        '''
        Search node in deep to count tags and amount of nested nodes
        '''
        acc = 1

        for descendant in filter(lambda x: isinstance(x, bs4.element.Tag), node):
            tag = descendant.name
            result.setdefault(tag, {'count': 0, 'nested': 0})
            result[tag]['count'] += 1
            children_num = Scrapper._traverse(descendant, result)
            acc += children_num
            result[tag]['nested'] += children_num - 1

        return acc

    def process(self) -> ResultType:
        '''
        Process document and return a result dictionary
        Instance should be initialized first

        :return: Dictionary with statistics
        '''
        result: ResultType = dict()

        self._traverse(self._data, result)

        return result
