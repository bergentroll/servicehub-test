# Pre-employment test for SERVICEHUB

## Run

To start a foreground service execute:
```
# docker-compose up wsgi
```

Service will be exposed at [localhost:8000](http://localhost:8000).

To run unit test cases:
```
# docker-compose up test
```

Use following command to stop containers running in background:
```
# docker-compose down
```

# REST API

Endpoints:
 - `{GET} /schema` — OpenAPI specification
 - `{POST} /tag_counter` — append a task to process page by URL, `url` parameter is obligatory, response contains
   a `task_id`
 - `{GET} /tag_counter/{task_id}` — get state and result of task by the `task_id`
